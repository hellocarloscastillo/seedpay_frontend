export const environment = {
  //url: "http://ec2-3-84-86-225.compute-1.amazonaws.com/api",
  url_useracount: "http://ec2-54-173-124-161.compute-1.amazonaws.com/UserAcountService/api",
  url_business: "http://ec2-54-173-124-161.compute-1.amazonaws.com/BussinesSerivice/api",
  url_storage: "http://ec2-54-173-124-161.compute-1.amazonaws.com/StorageService",
  //urlS3: "http://ec2-3-84-86-225.compute-1.amazonaws.com/storageService/uploadimage",
//  amazonS3: "http://sstorage-seedpay.s3.sa-east-1.amazonaws.com/",
  amazonS3:"https://seedpay-storage.s3.amazonaws.com/",
  production: false,
  instagram_token: 'INSTA_TOKEN',
  stripe_token: 'STRIPE_TOKEN',
  paypal_token: 'PAYPAL_TOKEN'
};
