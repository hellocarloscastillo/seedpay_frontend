import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';

import { CollectionSlider } from 'src/app/shared/data/slider';
import { Motivopqrsf } from './models/motivopqrsf';
import { Pqrsf } from './models/pqrsf';
import { PqrsfService } from './service/pqrsf.service';

@Component({
  templateUrl: './pqrsf.component.html',
  styleUrls: ['./pqrsf.component.scss']
})
export class PqrsfComponent implements OnInit {
  @Input() class: string;
  categories :any[] = []
  motivos: Motivopqrsf[] = []
  pqrsf:Pqrsf = {} as Pqrsf

  public CollectionSliderConfig: any = CollectionSlider;


  constructor(
    private pqrsfService: PqrsfService,
    private _toast: ToastrService,
  ) {
    this.traerMotivosPqrsf()
   }

  ngOnInit(): void {
  }


  traerMotivosPqrsf(){
    this.pqrsfService.obtenerMotivosPqrsf().subscribe({
      next: (data:Motivopqrsf[]) => {
        this.motivos = data
      },
      error: (error:any) => {

      }
    })
  }

  enviarPqrsf(){
    this.pqrsfService.guardarPqrsf(this.pqrsf).subscribe({
      next: (data:Pqrsf) => {
        this.pqrsf = data
        this._toast.info("Su solicitud ha sido enviada, nos pondremos encontacto con usted lo mas pronto pocible.", "Solicitud Enviada", {
          timeOut: 5000
        });
      },
      error: (error:any) => {
        this._toast.error(JSON.stringify(error.error), "Lo sentimos", {
          timeOut: 5000
        });
      }
    })
  }

}
