import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './politicas-terminos.component.html',
  styleUrls: ['./politicas-terminos.component.scss']
})
export class PoliticasTerminosComponent implements OnInit {

  terminos_politicas:boolean = false

  constructor() { }

  ngOnInit(): void {
  }

  change(value:boolean){
    this.terminos_politicas = value
  }

}
