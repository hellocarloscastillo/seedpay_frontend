import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { IPayPalConfig, ICreateOrderRequest } from 'ngx-paypal';
import { CarritoComprasLocalService } from 'src/app/services/carrito-compras-local.service';
import { Producto } from 'src/app/interfaces/producto';
import { OrderService } from 'src/app/shared/services/order.service';
import { environment } from 'src/environments/environment';
import { Departamento } from 'src/app/interfaces/departamento';
import { Ciudad } from 'src/app/interfaces/ciudad';
import { DepartamentoService } from 'src/app/services/departamento.service';
import { CiudadService } from 'src/app/services/ciudad.service';
import { Usuario } from 'src/app/interfaces/usuario';
import { TipoTransporte } from 'src/app/interfaces/tipoTransporte';
import { TipoTransporteService } from 'src/app/services/tipoTransporte.service';
import { PedidoVentaOnline, ProductoPedido } from 'src/app/interfaces/pedidoVentaOnline';
import { VentasOnline } from 'src/app/services/ventas-online.service';
import { ToastrService } from 'ngx-toastr';
declare const ePayco: any;
declare const testalert: any;


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],

})
export class CheckoutComponent implements OnInit {

  public checkoutForm: FormGroup;
  public products: Producto[] = [];
  public payPalConfig?: IPayPalConfig;
  public payment: string = 'Stripe';
  public amount: any;
  public departamentos: Departamento[];
  public ciudades: Ciudad[];
  public usuario: Usuario;
  public tiposTransporte: TipoTransporte[];
  public pedidoVentaOnline: PedidoVentaOnline = new PedidoVentaOnline();


  constructor(private fb: FormBuilder,
    public productService: CarritoComprasLocalService,
    private orderService: OrderService,
    private tipoTransporteService: TipoTransporteService,
    private ventaOnlineService: VentasOnline,
    private _toast: ToastrService
    ) {
    this.checkoutForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      lastname: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      phone: ['', [Validators.required, Validators.pattern('[0-9]+')]],
      email: ['', [Validators.required, Validators.email]],
      address: ['', [Validators.required, Validators.maxLength(50)]],
      metodoPago: ['', [Validators.required]],
      transporte: ['', [Validators.required]],
    });
    this.tipoTransporteService.getTiposTransporte().subscribe(response => {
      this.tiposTransporte = response;
    });
    this.usuario = JSON.parse(sessionStorage.getItem('user')) || {};
    this.pedidoVentaOnline.usuario = this.usuario;
    this.pedidoVentaOnline.total = this.getTotal();
    this.setCartItems();
  }

  ngOnInit(): void {
    this.products = this.productService.cartItems;
  }

  public getTotal(): number {
    return this.productService.cartTotalAmount();
  }

  getTotalSubstracIva(): number {
    let total = this.productService.cartTotalAmount()
    return total - (total * 0.19);
  }

  tipoTransporte(evt) {
    var target = evt.target;
    if (target.checked) {

    }
  }

  realizarPedido() {
    if(this.checkoutForm.valid){
      console.log("dats : " + JSON.stringify(this.pedidoVentaOnline))
      let handler = ePayco.checkout.configure({
        key: 'cb483f8903870c1df1599f4941257026',
        test: true
      })
      this.pedidoVentaOnline.transporte=1
      this.ventaOnlineService.responseEpayco(this.pedidoVentaOnline).subscribe(response =>{
        handler.open(response)
      });

      return true
    }else{
      this._toast.warning('El formulario esta incompleto', "Lo sentimos", {
        timeOut: 5000
      });
    }
  }

  setCartItems() {
    this.productService.cartItems.forEach(itm => {
      let productoCantidad: ProductoPedido = new ProductoPedido();
      productoCantidad.producto = itm.id;
      productoCantidad.cantidad = itm.quantity;
      this.pedidoVentaOnline.productos.push(productoCantidad);
    });
  }

}
