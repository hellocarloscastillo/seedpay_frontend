import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioSession } from 'src/app/interfaces/usuario-session';
import { UsuarioService } from 'src/app/services/usuario.service';
import { NavService } from '../../service/nav.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public right_sidebar: boolean = false;
  public open: boolean = false;
  public openNav: boolean = false;
  public isOpenMobile : boolean;
  usuarioSession:UsuarioSession = JSON.parse('{}')

  @Output() rightSidebarEvent = new EventEmitter<boolean>();

  constructor(
    private usuarioService:UsuarioService,
    public navServices: NavService,
    private _route:Router
    ) {
    this.usuarioSession = JSON.parse(sessionStorage.getItem('user') || '{}')
  }

  collapseSidebar() {
    this.open = !this.open;
    this.navServices.collapseSidebar = !this.navServices.collapseSidebar
  }
  right_side_bar() {
    this.right_sidebar = !this.right_sidebar
    this.rightSidebarEvent.emit(this.right_sidebar)
  }

  openMobileNav() {
    this.openNav = !this.openNav;
  }


  ngOnInit() {  }


  logout(){
    this.usuarioService.logoutUsuario(this.usuarioSession.id).subscribe(_response => {
      sessionStorage.removeItem('token')
      sessionStorage.removeItem('user')
      this._route.navigate(['inicio']);
    });
  }

}
